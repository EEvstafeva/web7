$(".slider").slick({
      infinite: true,
      dots: true,
      slidesToShow: 4,
      slidesToScroll: 4,

      responsive: [
            {
                  breakpoint: 768,
                  settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                  }
            },

            {
                  breakpoint: 480,
                  settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                  }
            }
      ]
    });

    
$('.center').slick({
      centerMode: true,
      slidesToShow: 3,
   });
